## Classes

<dl>
<dt><a href="#Apps">Apps</a> ⇐ <code><a href="#Collection">Collection</a></code></dt>
<dd><p>An App collection</p>
</dd>
<dt><a href="#Hosts">Hosts</a> ⇐ <code><a href="#Collection">Collection</a></code></dt>
<dd><p>A Host collection</p>
</dd>
<dt><a href="#Collection">Collection</a></dt>
<dd><p>A generic collection of models</p>
</dd>
<dt><a href="#GetJSON">GetJSON</a></dt>
<dd><p>A JSON getter helper</p>
</dd>
<dt><a href="#Model">Model</a></dt>
<dd><p>A simple item model</p>
</dd>
<dt><a href="#App">App</a> ⇐ <code><a href="#Model">Model</a></code></dt>
<dd><p>An App item Model</p>
</dd>
<dt><a href="#Host">Host</a> ⇐ <code><a href="#Model">Model</a></code></dt>
<dd><p>A Host item Model</p>
</dd>
<dt><a href="#HostTopApps">HostTopApps</a></dt>
<dd><p>A HostTopApps view</p>
</dd>
<dt><a href="#ApdexBoard">ApdexBoard</a></dt>
<dd><p>An ApdexBoard app</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#simpleCallback">simpleCallback</a> : <code>function</code></dt>
<dd><p>The <code>simpleCallback</code> is for asynchronous purposes, with no params</p>
</dd>
<dt><a href="#doneCallback">doneCallback</a> : <code>function</code></dt>
<dd><p>The <code>doneCallback</code> is executed when a request has been completed OK</p>
</dd>
<dt><a href="#errorCallback">errorCallback</a> : <code>function</code></dt>
<dd><p>The <code>errorCallback</code> is executed when a request fails</p>
</dd>
</dl>

<a name="Apps"></a>

## Apps ⇐ [<code>Collection</code>](#Collection)
An App collection

**Kind**: global class  
**Extends**: [<code>Collection</code>](#Collection)  

* [Apps](#Apps) ⇐ [<code>Collection</code>](#Collection)
    * [new Apps(items)](#new_Apps_new)
    * [.removeApp(app, options)](#Apps+removeApp) ⇒ [<code>App</code>](#App) \| <code>undefined</code>
    * [.addItems(items, options)](#Collection+addItems) ⇒ [<code>Collection</code>](#Collection)
    * [.addItem(item, options)](#Collection+addItem) ⇒ [<code>Model</code>](#Model)
    * [.findWhere(attr, value)](#Collection+findWhere) ⇒ [<code>Model</code>](#Model) \| <code>undefined</code>

<a name="new_Apps_new"></a>

### new Apps(items)
Create a new Apps collection


| Param | Type | Description |
| --- | --- | --- |
| items | <code>Array.&lt;Object&gt;</code> | The items to populate collection, each item can be an object or an App model instance |

<a name="Apps+removeApp"></a>

### apps.removeApp(app, options) ⇒ [<code>App</code>](#App) \| <code>undefined</code>
Remove an app from the collection

**Kind**: instance method of [<code>Apps</code>](#Apps)  
**Returns**: [<code>App</code>](#App) \| <code>undefined</code> - - The removed App if has been removed,
or undefined if no App was matching the given argument  

| Param | Type | Description |
| --- | --- | --- |
| app | [<code>App</code>](#App) \| <code>String</code> | An App model or an app name string |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+addItems"></a>

### apps.addItems(items, options) ⇒ [<code>Collection</code>](#Collection)
Adds the given items to the collection

**Kind**: instance method of [<code>Apps</code>](#Apps)  
**Returns**: [<code>Collection</code>](#Collection) - - Current collection  

| Param | Type | Description |
| --- | --- | --- |
| items | <code>Array.&lt;Object&gt;</code> | The items to populate collection, each item can be an object or a Model instance |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+addItem"></a>

### apps.addItem(item, options) ⇒ [<code>Model</code>](#Model)
Adds the given item to the collection

**Kind**: instance method of [<code>Apps</code>](#Apps)  
**Returns**: [<code>Model</code>](#Model) - - The added item Model  

| Param | Type | Description |
| --- | --- | --- |
| item | <code>Object</code> \| [<code>Model</code>](#Model) | The item to add, that can be an object or a Model instance |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+findWhere"></a>

### apps.findWhere(attr, value) ⇒ [<code>Model</code>](#Model) \| <code>undefined</code>
Find for an item matching the given criteria

**Kind**: instance method of [<code>Apps</code>](#Apps)  
**Returns**: [<code>Model</code>](#Model) \| <code>undefined</code> - - Matched item Model, or undefined if no
item was matching the given arguments  

| Param | Type | Description |
| --- | --- | --- |
| attr | <code>String</code> | The attribute to search for |
| value | <code>\*</code> | The value for given attribute to match |

<a name="Hosts"></a>

## Hosts ⇐ [<code>Collection</code>](#Collection)
A Host collection

**Kind**: global class  
**Extends**: [<code>Collection</code>](#Collection)  

* [Hosts](#Hosts) ⇐ [<code>Collection</code>](#Collection)
    * [new Hosts(items)](#new_Hosts_new)
    * [.addHostByName(hostName, options)](#Hosts+addHostByName) ⇒ [<code>Hosts</code>](#Hosts)
    * [.getHostByName(hostName)](#Hosts+getHostByName) ⇒ [<code>Host</code>](#Host) \| <code>undefined</code>
    * [.addAppToHosts(app, options)](#Hosts+addAppToHosts) ⇒ [<code>Hosts</code>](#Hosts)
    * [.addAppToHost(app, hostName, options)](#Hosts+addAppToHost) ⇒ [<code>Hosts</code>](#Hosts)
    * [.addItems(items, options)](#Collection+addItems) ⇒ [<code>Collection</code>](#Collection)
    * [.addItem(item, options)](#Collection+addItem) ⇒ [<code>Model</code>](#Model)
    * [.findWhere(attr, value)](#Collection+findWhere) ⇒ [<code>Model</code>](#Model) \| <code>undefined</code>

<a name="new_Hosts_new"></a>

### new Hosts(items)
Create a new Hosts collection


| Param | Type | Description |
| --- | --- | --- |
| items | <code>Array.&lt;Object&gt;</code> | The items to populate collection, each item can be an object or an App model instance |

<a name="Hosts+addHostByName"></a>

### hosts.addHostByName(hostName, options) ⇒ [<code>Hosts</code>](#Hosts)
Adds a new Host to collection by giving just the name

**Kind**: instance method of [<code>Hosts</code>](#Hosts)  
**Returns**: [<code>Hosts</code>](#Hosts) - - Current Hosts collection  

| Param | Type | Description |
| --- | --- | --- |
| hostName | <code>String</code> |  |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Hosts+getHostByName"></a>

### hosts.getHostByName(hostName) ⇒ [<code>Host</code>](#Host) \| <code>undefined</code>
Returns a Host matching the given name, if exists

**Kind**: instance method of [<code>Hosts</code>](#Hosts)  
**Returns**: [<code>Host</code>](#Host) \| <code>undefined</code> - - Matched host, or undefined if no
Host was matching the given argument  

| Param | Type |
| --- | --- |
| hostName | <code>String</code> | 

<a name="Hosts+addAppToHosts"></a>

### hosts.addAppToHosts(app, options) ⇒ [<code>Hosts</code>](#Hosts)
Adds an App to every Host the App is hosted

**Kind**: instance method of [<code>Hosts</code>](#Hosts)  
**Returns**: [<code>Hosts</code>](#Hosts) - - Current Hosts collection  

| Param | Type | Description |
| --- | --- | --- |
| app | [<code>App</code>](#App) |  |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Hosts+addAppToHost"></a>

### hosts.addAppToHost(app, hostName, options) ⇒ [<code>Hosts</code>](#Hosts)
Adds an App to a Host specified by name

**Kind**: instance method of [<code>Hosts</code>](#Hosts)  
**Returns**: [<code>Hosts</code>](#Hosts) - - Current Hosts collection  

| Param | Type | Description |
| --- | --- | --- |
| app | [<code>App</code>](#App) |  |
| hostName | <code>String</code> |  |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+addItems"></a>

### hosts.addItems(items, options) ⇒ [<code>Collection</code>](#Collection)
Adds the given items to the collection

**Kind**: instance method of [<code>Hosts</code>](#Hosts)  
**Returns**: [<code>Collection</code>](#Collection) - - Current collection  

| Param | Type | Description |
| --- | --- | --- |
| items | <code>Array.&lt;Object&gt;</code> | The items to populate collection, each item can be an object or a Model instance |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+addItem"></a>

### hosts.addItem(item, options) ⇒ [<code>Model</code>](#Model)
Adds the given item to the collection

**Kind**: instance method of [<code>Hosts</code>](#Hosts)  
**Returns**: [<code>Model</code>](#Model) - - The added item Model  

| Param | Type | Description |
| --- | --- | --- |
| item | <code>Object</code> \| [<code>Model</code>](#Model) | The item to add, that can be an object or a Model instance |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+findWhere"></a>

### hosts.findWhere(attr, value) ⇒ [<code>Model</code>](#Model) \| <code>undefined</code>
Find for an item matching the given criteria

**Kind**: instance method of [<code>Hosts</code>](#Hosts)  
**Returns**: [<code>Model</code>](#Model) \| <code>undefined</code> - - Matched item Model, or undefined if no
item was matching the given arguments  

| Param | Type | Description |
| --- | --- | --- |
| attr | <code>String</code> | The attribute to search for |
| value | <code>\*</code> | The value for given attribute to match |

<a name="Collection"></a>

## Collection
A generic collection of models

**Kind**: global class  

* [Collection](#Collection)
    * [new Collection(Model, items)](#new_Collection_new)
    * [.addItems(items, options)](#Collection+addItems) ⇒ [<code>Collection</code>](#Collection)
    * [.addItem(item, options)](#Collection+addItem) ⇒ [<code>Model</code>](#Model)
    * [.findWhere(attr, value)](#Collection+findWhere) ⇒ [<code>Model</code>](#Model) \| <code>undefined</code>

<a name="new_Collection_new"></a>

### new Collection(Model, items)
Creates a new collection


| Param | Type | Description |
| --- | --- | --- |
| Model | [<code>Model</code>](#Model) | The Model constructor of each collection item |
| items | <code>Array.&lt;Object&gt;</code> | The items to populate collection, each item can be an object or a Model instance |

<a name="Collection+addItems"></a>

### collection.addItems(items, options) ⇒ [<code>Collection</code>](#Collection)
Adds the given items to the collection

**Kind**: instance method of [<code>Collection</code>](#Collection)  
**Returns**: [<code>Collection</code>](#Collection) - - Current collection  

| Param | Type | Description |
| --- | --- | --- |
| items | <code>Array.&lt;Object&gt;</code> | The items to populate collection, each item can be an object or a Model instance |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+addItem"></a>

### collection.addItem(item, options) ⇒ [<code>Model</code>](#Model)
Adds the given item to the collection

**Kind**: instance method of [<code>Collection</code>](#Collection)  
**Returns**: [<code>Model</code>](#Model) - - The added item Model  

| Param | Type | Description |
| --- | --- | --- |
| item | <code>Object</code> \| [<code>Model</code>](#Model) | The item to add, that can be an object or a Model instance |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Collection+findWhere"></a>

### collection.findWhere(attr, value) ⇒ [<code>Model</code>](#Model) \| <code>undefined</code>
Find for an item matching the given criteria

**Kind**: instance method of [<code>Collection</code>](#Collection)  
**Returns**: [<code>Model</code>](#Model) \| <code>undefined</code> - - Matched item Model, or undefined if no
item was matching the given arguments  

| Param | Type | Description |
| --- | --- | --- |
| attr | <code>String</code> | The attribute to search for |
| value | <code>\*</code> | The value for given attribute to match |

<a name="GetJSON"></a>

## GetJSON
A JSON getter helper

**Kind**: global class  

* [GetJSON](#GetJSON)
    * [new GetJSON(url)](#new_GetJSON_new)
    * [.get(done, err)](#GetJSON+get)
    * [.fetch(done, err)](#GetJSON+fetch)
    * [.xhr(done, err)](#GetJSON+xhr)

<a name="new_GetJSON_new"></a>

### new GetJSON(url)
Creates a GetJSON instance


| Param | Type | Description |
| --- | --- | --- |
| url | <code>String</code> | The url where to fetch the resource |

<a name="GetJSON+get"></a>

### getJSON.get(done, err)
Execute the fetching process

**Kind**: instance method of [<code>GetJSON</code>](#GetJSON)  

| Param | Type | Description |
| --- | --- | --- |
| done | [<code>doneCallback</code>](#doneCallback) | The callback with the response |
| err | [<code>errorCallback</code>](#errorCallback) | The callback with error |

<a name="GetJSON+fetch"></a>

### getJSON.fetch(done, err)
Execute the fetching process by using fetch

**Kind**: instance method of [<code>GetJSON</code>](#GetJSON)  

| Param | Type | Description |
| --- | --- | --- |
| done | [<code>doneCallback</code>](#doneCallback) | The callback with the response |
| err | [<code>errorCallback</code>](#errorCallback) | The callback with error |

<a name="GetJSON+xhr"></a>

### getJSON.xhr(done, err)
Execute the fetching process by using XMLHttpRequest

**Kind**: instance method of [<code>GetJSON</code>](#GetJSON)  

| Param | Type | Description |
| --- | --- | --- |
| done | [<code>doneCallback</code>](#doneCallback) | The callback with the response |
| err | [<code>errorCallback</code>](#errorCallback) | The callback with error |

<a name="Model"></a>

## Model
A simple item model

**Kind**: global class  
<a name="new_Model_new"></a>

### new Model(defaultAttributes, attributes)
Creates a new model


| Param | Type | Description |
| --- | --- | --- |
| defaultAttributes | <code>Object</code> | The default attributes as fallback |
| attributes | <code>Object</code> | The Model attributes |

<a name="App"></a>

## App ⇐ [<code>Model</code>](#Model)
An App item Model

**Kind**: global class  
**Extends**: [<code>Model</code>](#Model)  

* [App](#App) ⇐ [<code>Model</code>](#Model)
    * [new App(attributes)](#new_App_new)
    * [.getHostNames()](#App+getHostNames) ⇒ <code>Array.&lt;String&gt;</code>

<a name="new_App_new"></a>

### new App(attributes)
Create a new App model


| Param | Type | Description |
| --- | --- | --- |
| attributes | <code>Object</code> | The App attributes |
| attributes.name | <code>String</code> |  |
| attributes.contributors | <code>Array.&lt;String&gt;</code> |  |
| attributes.version | <code>Number</code> |  |
| attributes.apdex | <code>Number</code> |  |
| attributes.host | <code>Array.&lt;String&gt;</code> | The hosts where the App is hosted |

<a name="App+getHostNames"></a>

### app.getHostNames() ⇒ <code>Array.&lt;String&gt;</code>
Get the hosts where the App is hosted

**Kind**: instance method of [<code>App</code>](#App)  
**Returns**: <code>Array.&lt;String&gt;</code> - The hosts where the App is hosted (attributes.host)  
<a name="Host"></a>

## Host ⇐ [<code>Model</code>](#Model)
A Host item Model

**Kind**: global class  
**Extends**: [<code>Model</code>](#Model)  

* [Host](#Host) ⇐ [<code>Model</code>](#Model)
    * [new Host(attributes)](#new_Host_new)
    * [.addApp(app, options)](#Host+addApp) ⇒ [<code>App</code>](#App)
    * [.removeApp(app, options)](#Host+removeApp) ⇒ [<code>App</code>](#App) \| <code>undefined</code>
    * [.getTopApps(maxResults)](#Host+getTopApps) ⇒ [<code>Array.&lt;App&gt;</code>](#App)

<a name="new_Host_new"></a>

### new Host(attributes)
Create a new Host model


| Param | Type | Description |
| --- | --- | --- |
| attributes | <code>Object</code> | The Host attributes |
| attributes.name | <code>String</code> |  |

<a name="Host+addApp"></a>

### host.addApp(app, options) ⇒ [<code>App</code>](#App)
Adds an App to the Host Apps collection

**Kind**: instance method of [<code>Host</code>](#Host)  
**Returns**: [<code>App</code>](#App) - The added App  

| Param | Type | Description |
| --- | --- | --- |
| app | [<code>App</code>](#App) | The App to add |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Host+removeApp"></a>

### host.removeApp(app, options) ⇒ [<code>App</code>](#App) \| <code>undefined</code>
Removes an App from the Host Apps collection, if exists

**Kind**: instance method of [<code>Host</code>](#Host)  
**Returns**: [<code>App</code>](#App) \| <code>undefined</code> - - The removed App if has been removed,
or undefined if no App was matching the given argument  

| Param | Type | Description |
| --- | --- | --- |
| app | [<code>App</code>](#App) | The App to add |
| options | <code>Object</code> | Options object |
| options.silent | <code>Boolean</code> | If true, does not fire events |

<a name="Host+getTopApps"></a>

### host.getTopApps(maxResults) ⇒ [<code>Array.&lt;App&gt;</code>](#App)
Retrieves the top Apps from Host based on best apdex score

**Kind**: instance method of [<code>Host</code>](#Host)  
**Returns**: [<code>Array.&lt;App&gt;</code>](#App) - A sorted list with the top Apps by apdex  

| Param | Type | Description |
| --- | --- | --- |
| maxResults | <code>Number</code> | The maximum results to retrieve |

<a name="HostTopApps"></a>

## HostTopApps
A HostTopApps view

**Kind**: global class  

* [HostTopApps](#HostTopApps)
    * [new HostTopApps(model, options)](#new_HostTopApps_new)
    * [.render()](#HostTopApps+render) ⇒ [<code>HostTopApps</code>](#HostTopApps)
    * [.bindEvents()](#HostTopApps+bindEvents) ⇒ [<code>HostTopApps</code>](#HostTopApps)
    * [.unbindEvents()](#HostTopApps+unbindEvents) ⇒ [<code>HostTopApps</code>](#HostTopApps)
    * [._clickEvent()](#HostTopApps+_clickEvent)

<a name="new_HostTopApps_new"></a>

### new HostTopApps(model, options)
Create a new HostTopApps view, including render and events binding


| Param | Type | Description |
| --- | --- | --- |
| model | [<code>Host</code>](#Host) | The Host model who to retrieve top Apps |
| options | <code>Object</code> | The view options |
| options.apps | <code>Number</code> | The number of apps to show |
| options.tagName | <code>String</code> | The HTMLElement tag used on view element |
| options.classNames | <code>Array.&lt;String&gt;</code> | Class names to add to view element |
| options.appEventAttribute | <code>String</code> | The attribute to identify a shown app and catch view delegated events |
| options.template | <code>function</code> | The template function that returns the view innerHTML for the given data |

<a name="HostTopApps+render"></a>

### hostTopApps.render() ⇒ [<code>HostTopApps</code>](#HostTopApps)
Renders the view to the View element property with the Host data.
If the element is not created yet, it creates a new one.

**Kind**: instance method of [<code>HostTopApps</code>](#HostTopApps)  
**Returns**: [<code>HostTopApps</code>](#HostTopApps) - The current view  
<a name="HostTopApps+bindEvents"></a>

### hostTopApps.bindEvents() ⇒ [<code>HostTopApps</code>](#HostTopApps)
Bind the click event to the View element property, and listen for
the Host Apps collection update to refresh the view using render.
Since is using event delegation for the click event, there's no
needing for rebinding events after a render.

**Kind**: instance method of [<code>HostTopApps</code>](#HostTopApps)  
**Returns**: [<code>HostTopApps</code>](#HostTopApps) - The current view  
<a name="HostTopApps+unbindEvents"></a>

### hostTopApps.unbindEvents() ⇒ [<code>HostTopApps</code>](#HostTopApps)
Unbind the events bound with bindEvents

**Kind**: instance method of [<code>HostTopApps</code>](#HostTopApps)  
**Returns**: [<code>HostTopApps</code>](#HostTopApps) - The current view  
<a name="HostTopApps+_clickEvent"></a>

### hostTopApps._clickEvent()
Caught a click event on any App, alerts with App info

**Kind**: instance method of [<code>HostTopApps</code>](#HostTopApps)  
**Params**: <code>Event</code> ev - The captured event  
<a name="ApdexBoard"></a>

## ApdexBoard
An ApdexBoard app

**Kind**: global class  

* [ApdexBoard](#ApdexBoard)
    * [new ApdexBoard(el, options, apps)](#new_ApdexBoard_new)
    * _instance_
        * [.getTopAppsByHost(hostName, appsAmount)](#ApdexBoard+getTopAppsByHost) ⇒ [<code>Array.&lt;App&gt;</code>](#App)
        * [.addAppToHosts(app)](#ApdexBoard+addAppToHosts) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [.removeAppFromHosts(app)](#ApdexBoard+removeAppFromHosts) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [.bindEvents()](#ApdexBoard+bindEvents) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [.unbindEvents()](#ApdexBoard+unbindEvents) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [._initElements(el)](#ApdexBoard+_initElements) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [._initProperties(options)](#ApdexBoard+_initProperties) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [._initApps(apps, done)](#ApdexBoard+_initApps) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [._initHosts()](#ApdexBoard+_initHosts) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [._initHostsTopAppsViews()](#ApdexBoard+_initHostsTopAppsViews) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [._appendHostsTopAppsViews()](#ApdexBoard+_appendHostsTopAppsViews) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
        * [._fetch(done)](#ApdexBoard+_fetch) ⇒ [<code>GetJSON</code>](#GetJSON)
        * [._fetched(done)](#ApdexBoard+_fetched)
        * [._displayToggle(ev)](#ApdexBoard+_displayToggle)
    * _static_
        * [.dataApi(window, document)](#ApdexBoard.dataApi)

<a name="new_ApdexBoard_new"></a>

### new ApdexBoard(el, options, apps)
Create a new ApdexBoard app, including fetching data and rendering views


| Param | Type | Description |
| --- | --- | --- |
| el | <code>HTMLElement</code> | The HTMLElement to attach the ApdexBoard app behaviour |
| options | <code>Object</code> | The ApdexBoard options |
| options.appsByHost | <code>Number</code> | The number of apps to show for each Host |
| options.dataSrc | <code>String</code> | The URL to fetch Apps JSON data from |
| options.unresolvedStateClassName | <code>String</code> | Unresolved element class name* |
| options.gridViewClassName | <code>String</code> | Unresolved element class name |
| apps | [<code>Apps</code>](#Apps) \| <code>Array.&lt;Object&gt;</code> | The complete Apps collection to display as Hosts top Apps |

<a name="ApdexBoard+getTopAppsByHost"></a>

### apdexBoard.getTopAppsByHost(hostName, appsAmount) ⇒ [<code>Array.&lt;App&gt;</code>](#App)
Returns certain amount of top Apps for a given host name

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>Array.&lt;App&gt;</code>](#App) - top apps retrieved from host  

| Param | Type |
| --- | --- |
| hostName | <code>String</code> | 
| appsAmount | <code>Number</code> | 

<a name="ApdexBoard+addAppToHosts"></a>

### apdexBoard.addAppToHosts(app) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Given an App, is added to:
- ApdexBoard Apps collection
- Every Host from ApdexBoard Hosts collection

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  

| Param | Type | Description |
| --- | --- | --- |
| app | [<code>App</code>](#App) \| <code>Object</code> | An App model or an app object |

<a name="ApdexBoard+removeAppFromHosts"></a>

### apdexBoard.removeAppFromHosts(app) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Given an App, is removed from:
- Every Host from ApdexBoard Hosts collection
- ApdexBoard Apps collection

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  

| Param | Type | Description |
| --- | --- | --- |
| app | [<code>App</code>](#App) \| <code>String</code> | An App model or an app name string |

<a name="ApdexBoard+bindEvents"></a>

### apdexBoard.bindEvents() ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Bind the following events:
- DisplayToggleElement > Change

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  
<a name="ApdexBoard+unbindEvents"></a>

### apdexBoard.unbindEvents() ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Unbind the events bound with bindEvents

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  
<a name="ApdexBoard+_initElements"></a>

### apdexBoard._initElements(el) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Init the HTMLElements references to run the app

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  

| Param | Type | Description |
| --- | --- | --- |
| el | <code>HTMLElement</code> | Main app element |

<a name="ApdexBoard+_initProperties"></a>

### apdexBoard._initProperties(options) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Init the properties needed to run the app

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Main app options |

<a name="ApdexBoard+_initApps"></a>

### apdexBoard._initApps(apps, done) ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Initializes the Apps collection, using given apps data or fetching it, if missing

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  

| Param | Type | Description |
| --- | --- | --- |
| apps | [<code>Apps</code>](#Apps) \| <code>Array.&lt;Object&gt;</code> | The Apps collection, or an array containing app objects |
| done | [<code>simpleCallback</code>](#simpleCallback) | Executed when the App collection is ready and populated |

<a name="ApdexBoard+_initHosts"></a>

### apdexBoard._initHosts() ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Initializes the Hosts collection, and add every available App to them

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  
<a name="ApdexBoard+_initHostsTopAppsViews"></a>

### apdexBoard._initHostsTopAppsViews() ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Creates a HostTopApps view for every Host, and save it as view Host property

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  
<a name="ApdexBoard+_appendHostsTopAppsViews"></a>

### apdexBoard._appendHostsTopAppsViews() ⇒ [<code>ApdexBoard</code>](#ApdexBoard)
Append every HostTopApps view to the content element, and removes the unresolved state

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>ApdexBoard</code>](#ApdexBoard) - - Current ApdexBoard instance  
<a name="ApdexBoard+_fetch"></a>

### apdexBoard._fetch(done) ⇒ [<code>GetJSON</code>](#GetJSON)
Creates a GetJSON instance and starts fetching data from dataSrc option url

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  
**Returns**: [<code>GetJSON</code>](#GetJSON) - - Current GetJSON instance  

| Param | Type | Description |
| --- | --- | --- |
| done | [<code>doneCallback</code>](#doneCallback) | The callback with the response |

<a name="ApdexBoard+_fetched"></a>

### apdexBoard._fetched(done)
Emits the fetched event and executes given callback

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  

| Param | Type | Description |
| --- | --- | --- |
| done | [<code>simpleCallback</code>](#simpleCallback) \| [<code>doneCallback</code>](#doneCallback) | Executed after firing events |

<a name="ApdexBoard+_displayToggle"></a>

### apdexBoard._displayToggle(ev)
Toggle the contentElement view from grid to list, according the eventTarget checked property

**Kind**: instance method of [<code>ApdexBoard</code>](#ApdexBoard)  

| Param | Type | Description |
| --- | --- | --- |
| ev | <code>Event</code> | Event fired from a checkbox input |

<a name="ApdexBoard.dataApi"></a>

### ApdexBoard.dataApi(window, document)
Starts a new ApdexBoard app instance to HTMLElements who matches certain attributes

**Kind**: static method of [<code>ApdexBoard</code>](#ApdexBoard)  

| Param | Type | Description |
| --- | --- | --- |
| window | <code>Window</code> | Window reference |
| document | <code>Document</code> | Document reference |

<a name="simpleCallback"></a>

## simpleCallback : <code>function</code>
The `simpleCallback` is for asynchronous purposes, with no params

**Kind**: global typedef  
<a name="doneCallback"></a>

## doneCallback : <code>function</code>
The `doneCallback` is executed when a request has been completed OK

**Kind**: global typedef  

| Param | Type |
| --- | --- |
| responseData | <code>Object</code> | 

<a name="errorCallback"></a>

## errorCallback : <code>function</code>
The `errorCallback` is executed when a request fails

**Kind**: global typedef  

| Param | Type |
| --- | --- |
| response | <code>Object</code> | 

