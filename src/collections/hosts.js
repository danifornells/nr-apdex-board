const Collection = require('../helpers/collection')
const Host = require('../models/host')

/**
 * A Host collection
 * @extends Collection
 */
class Hosts extends Collection {

  /**
   * Create a new Hosts collection
   * @constructor
   * @param {Object[]} items - The items to populate collection,
   * each item can be an object or an App model instance
   */
  constructor (items) {
    super(Host, items)
  }

  /**
   * Adds a new Host to collection by giving just the name
   * @param {String} hostName
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {Hosts} - Current Hosts collection
   */
  addHostByName (hostName, options) {
    return this.addItem({name: hostName}, options)
  }

  /**
   * Returns a Host matching the given name, if exists
   * @param {String} hostName
   * @returns {Host|undefined} - Matched host, or undefined if no
   * Host was matching the given argument
   */
  getHostByName (hostName) {
    return this.findWhere('name', hostName)
  }

  /**
   * Adds an App to every Host the App is hosted
   * @param {App} app
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {Hosts} - Current Hosts collection
   */
  addAppToHosts (app, options) {
    app.getHostNames().forEach(hostName => this.addAppToHost(app, hostName, options))
    return this
  }

  /**
   * Adds an App to a Host specified by name
   * @param {App} app
   * @param {String} hostName
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {Hosts} - Current Hosts collection
   */
  addAppToHost (app, hostName, options) {
    let host = this.getHostByName(hostName) || this.addHostByName(hostName)
    host.addApp(app, options)
    return this
  }
}

module.exports = Hosts
