const Collection = require('../helpers/collection')
const App = require('../models/app')

/**
 * An App collection
 * @extends Collection
 */
class Apps extends Collection {

  /**
   * Create a new Apps collection
   * @constructor
   * @param {Object[]} items - The items to populate collection,
   * each item can be an object or an App model instance
   */
  constructor (items) {
    super(App, items)
  }

  /**
   * Remove an app from the collection
   * @param {App|String} app - An App model or an app name string
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {App|undefined} - The removed App if has been removed,
   * or undefined if no App was matching the given argument
   */
  removeApp (app, options) {
    // If the app is an existing model in collection, just splice
    if (app instanceof App && this.models.indexOf(app) > -1) {
      this.models.splice(this.models.indexOf(app), 1)
      if (!options || !options.silent) {
        this.events.emit('removed')
        this.events.emit('updated')
      }
      return app
    }

    // Resolve the appName by the given app argument, and proceed if there's a candidate to be removed
    const appName = (typeof app === 'string') ? app : (app instanceof App) ? app.attributes.name : app.name
    const appCandidate = this.findWhere('name', appName)
    if (appCandidate) {
      this.models.splice(this.models.indexOf(appCandidate), 1)
      if (!options || !options.silent) {
        this.events.emit('removed')
        this.events.emit('updated')
      }
      return appCandidate
    }
  }
}

module.exports = Apps
