/**
 * A JSON getter helper
 */
class GetJSON {

  /**
   * Creates a GetJSON instance
   * @constructor
   * @param {String} url - The url where to fetch the resource
   */
  constructor (url) {
    this.url = url
  }

  /**
   * Execute the fetching process
   * @param {doneCallback} done - The callback with the response
   * @param {errorCallback} err - The callback with error
   */
  get (done, err) {
    return (window.fetch ? this.fetch(done, err) : this.xhr(done, err))
  }

  /**
   * Execute the fetching process by using fetch
   * @param {doneCallback} done - The callback with the response
   * @param {errorCallback} err - The callback with error
   */
  fetch (done, err) {
    window.fetch(this.url)
      .then(response => {
        if (response.ok) {
          response.json().then(done)
        } else {
          err(response)
        }
      })
      .catch(err)
    return this
  }

  /**
   * Execute the fetching process by using XMLHttpRequest
   * @param {doneCallback} done - The callback with the response
   * @param {errorCallback} err - The callback with error
   */
  xhr (done, err) {
    let xhr = new window.XMLHttpRequest()
    xhr.open('GET', this.url)
    xhr.onload = function () {
      if (xhr.status === 200) {
        done(JSON.parse(xhr.responseText))
      } else {
        err(xhr.status)
      }
    }
    xhr.send()
    return this
  }
}

module.exports = GetJSON
