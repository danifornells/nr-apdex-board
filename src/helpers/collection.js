const EventEmitter = require('eventemitter3')
require('../polyfills/array-find')

/**
 * A generic collection of models
 */
class Collection {

  /**
   * Creates a new collection
   * @constructor
   * @param {Model} Model - The Model constructor of each collection item
   * @param {Object[]} items - The items to populate collection,
   * each item can be an object or a Model instance
   */
  constructor (Model, items) {
    this.models = []
    this.Model = Model
    this.events = new EventEmitter()
    if (items) this.addItems(items, {silent: true})
  }

  /**
   * Adds the given items to the collection
   * @param {Object[]} items - The items to populate collection,
   * each item can be an object or a Model instance
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {Collection} - Current collection
   */
  addItems (items, options) {
    items.forEach(item => this.addItem(item, {silent: true}))
    if (!options || !options.silent) {
      this.events.emit('added')
      this.events.emit('updated')
    }
    return this
  }

  /**
   * Adds the given item to the collection
   * @param {Object|Model} item - The item to add, that can be
   * an object or a Model instance
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {Model} - The added item Model
   */
  addItem (item, options) {
    const itemToAdd = item instanceof this.Model ? item : new this.Model(item)
    this.models.push(itemToAdd)
    if (!options || !options.silent) {
      this.events.emit('added')
      this.events.emit('updated')
    }
    return itemToAdd
  }

  /**
   * Find for an item matching the given criteria
   * @param {String} attr - The attribute to search for
   * @param {*} value - The value for given attribute to match
   * @returns {Model|undefined} - Matched item Model, or undefined if no
   * item was matching the given arguments
   */
  findWhere (attr, value) {
    return this.models.find(model => { return model.attributes[attr] === value })
  }
}

module.exports = Collection
