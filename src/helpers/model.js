/**
 * A simple item model
 */
class Model {

  /**
   * Creates a new model
   * @constructor
   * @param {Object} defaultAttributes - The default attributes as fallback
   * @param {Object} attributes - The Model attributes
   */
  constructor (defaultAttributes, attributes) {
    this.attributes = {}
    Object.keys(defaultAttributes).forEach(key => {
      this.attributes[key] = attributes[key] || defaultAttributes[key]
    })
  }
}

module.exports = Model
