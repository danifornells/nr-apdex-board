const Model = require('../helpers/model')

const defaults = {
  name: null,
  contributors: [],
  version: null,
  apdex: null,
  host: []
}

/**
 * An App item Model
 * @extends Model
 */
class App extends Model {

  /**
   * Create a new App model
   * @constructor
   * @param {Object} attributes - The App attributes
   * @param {String} attributes.name
   * @param {String[]} attributes.contributors
   * @param {Number} attributes.version
   * @param {Number} attributes.apdex
   * @param {String[]} attributes.host - The hosts where the App is hosted
   */
  constructor (attributes) {
    super(defaults, attributes)
  }

  /**
   * Get the hosts where the App is hosted
   * @returns {String[]} The hosts where the App is hosted (attributes.host)
   */
  getHostNames () {
    return this.attributes.host
  }
}

module.exports = App
