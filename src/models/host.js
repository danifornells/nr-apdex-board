const Model = require('../helpers/model')
const Apps = require('../collections/apps')

const defaults = {
  name: null
}

/**
 * A Host item Model
 * @extends Model
 */
class Host extends Model {

  /**
   * Create a new Host model
   * @constructor
   * @param {Object} attributes - The Host attributes
   * @param {String} attributes.name
   */
  constructor (attributes) {
    super(defaults, attributes)
    this.apps = new Apps()
  }

  /**
   * Adds an App to the Host Apps collection
   * @param {App} app - The App to add
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {App} The added App
   */
  addApp (app, options) {
    return this.apps.addItem(app, options)
  }

  /**
   * Removes an App from the Host Apps collection, if exists
   * @param {App} app - The App to add
   * @param {Object} options - Options object
   * @param {Boolean} options.silent - If true, does not fire events
   * @returns {App|undefined} - The removed App if has been removed,
   * or undefined if no App was matching the given argument
   */
  removeApp (app, options) {
    return this.apps.removeApp(app, options)
  }

  /**
   * Retrieves the top Apps from Host based on best apdex score
   * @param {Number} maxResults - The maximum results to retrieve
   * @returns {App[]} A sorted list with the top Apps by apdex
   */
  getTopApps (maxResults) {
    const appsByApdex = this.apps.models.slice(0).sort((a, b) => {
      return b.attributes.apdex - a.attributes.apdex
    })
    return appsByApdex.slice(0, maxResults)
  }
}

module.exports = Host
