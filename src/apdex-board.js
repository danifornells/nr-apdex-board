/**
 * The `simpleCallback` is for asynchronous purposes, with no params
 * @callback simpleCallback
 */

/**
 * The `doneCallback` is executed when a request has been completed OK
 * @callback doneCallback
 * @param {Object} responseData
 */

/**
 * The `errorCallback` is executed when a request fails
 * @callback errorCallback
 * @param {Object} response
 */

const EventEmitter = require('eventemitter3')
const GetJSON = require('./helpers/get-json')

const Apps = require('./collections/apps')
const Hosts = require('./collections/hosts')
const HostTopApps = require('./views/host-top-apps')

const appSettings = {
  elAPI: 'apdexBoard',
  elAttributesAPI: {
    dataSrc: 'data-apdex-board__data-src',
    content: 'data-apdex-board__content',
    displayToggle: 'data-apdex-board__display-toggle'
  }
}

const defaultOptions = {
  appsByHost: 5,
  dataSrc: null,
  unresolvedStateClassName: 'is-unresolved',
  gridViewClassName: 'o-layout--items-medium'
}

/**
 * An ApdexBoard app
 */
class ApdexBoard {

  /**
   * Create a new ApdexBoard app, including fetching data and rendering views
   *
   * @constructor
   *
   * @param {HTMLElement} el - The HTMLElement to attach the ApdexBoard app behaviour
   *
   * @param {Object} options - The ApdexBoard options
   * @param {Number} options.appsByHost - The number of apps to show for each Host
   * @param {String} options.dataSrc - The URL to fetch Apps JSON data from
   * @param {String} options.unresolvedStateClassName - Unresolved element class name*
   * @param {String} options.gridViewClassName - Unresolved element class name
   *
   * @param {Apps|Object[]} apps - The complete Apps collection to display as Hosts top Apps
   */
  constructor (el, options, apps) {
    // Check requirements
    if (!el || !(el instanceof window.HTMLElement)) throw new Error('ApdexBoard requires a HTMLElement as first argument')
    if (el[appSettings.elAPI] !== undefined) throw new Error('Given element already has an ApdexBoard instance associated')
    // Initialize the options and properties
    this
      ._initElements(el)
      ._initProperties(options)
    // Initialize data and append views
    this._initApps(apps, function () {
      this
        ._initHosts()
        ._initHostsTopAppsViews()
        ._appendHostsTopAppsViews()
    }.bind(this))
    // Bind events
    this.bindEvents()
    // Expose the API to the element
    this.element[appSettings.elAPI] = {
      element: this.element,
      contentElement: this.contentElement,
      options: this.options,
      events: this.events,
      fetched: this.fetched,
      apps: this.apps,
      hosts: this.hosts
    }
  }


  /**
   * Starts a new ApdexBoard app instance to HTMLElements who matches certain attributes
   * @param {Window} window - Window reference
   * @param {Document} document - Document reference
   */
  static dataApi (window, document) {
    const components = document.querySelectorAll(`[data-js-api="${appSettings.elAPI}"]`);
    [].forEach.call(components, function (el) {
      let appOptions = {}
      Object.keys(appSettings.elAttributesAPI).forEach(key => {
        if (el.hasAttribute(appSettings.elAttributesAPI[key])) { appOptions[key] = el.getAttribute(appSettings.elAttributesAPI[key]) }
      })
      window.apdexBoard = new ApdexBoard(el, appOptions)
    })
  }

  /**
   * Returns certain amount of top Apps for a given host name
   * @param {String} hostName
   * @param {Number} appsAmount
   * @returns {App[]} top apps retrieved from host
   */
  getTopAppsByHost (hostName, appsAmount) {
    appsAmount = appsAmount || 25
    return this.hosts.getHostByName(hostName).getTopApps(appsAmount)
  }

  /**
   * Given an App, is added to:
   * - ApdexBoard Apps collection
   * - Every Host from ApdexBoard Hosts collection
   * @param {App|Object} app - An App model or an app object
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  addAppToHosts (app) {
    const newApp = this.apps.addItem(app)
    this.hosts.addAppToHosts(newApp)
    return this
  }

  /**
   * Given an App, is removed from:
   * - Every Host from ApdexBoard Hosts collection
   * - ApdexBoard Apps collection
   * @param {App|String} app - An App model or an app name string
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  removeAppFromHosts (app) {
    this.hosts.models.forEach(host => {
      host.removeApp(app)
    })
    this.apps.removeApp(app)
    return this
  }

  /**
   * Bind the following events:
   * - DisplayToggleElement > Change
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  bindEvents () {
    if (this.displayToggleElement) { this.displayToggleElement.onchange = this._displayToggle.bind(this) }
    return this
  }

  /**
   * Unbind the events bound with bindEvents
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  unbindEvents () {
    if (this.displayToggleElement) { this.displayToggleElement.onchange = null }
    return this
  }

  /**
   * Init the HTMLElements references to run the app
   * @param {HTMLElement} el - Main app element
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  _initElements (el) {
    this.element = el
    this.contentElement = el.querySelector(`[${appSettings.elAttributesAPI.content}]`) || this.element
    this.displayToggleElement = el.querySelector(`[${appSettings.elAttributesAPI.displayToggle}]`)
    return this
  }

  /**
   * Init the properties needed to run the app
   * @param {Object} options - Main app options
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  _initProperties (options) {
    this.options = {}
    Object.keys(defaultOptions).forEach(key => {
      this.options[key] = options ? options[key] || defaultOptions[key] : defaultOptions[key]
    })
    this.fetched = false
    this.events = new EventEmitter()
    return this
  }

  /**
   * Initializes the Apps collection, using given apps data or fetching it, if missing
   * @param {Apps|Object[]} apps - The Apps collection, or an array containing app objects
   * @param {simpleCallback} done - Executed when the App collection is ready and populated
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  _initApps (apps, done) {
    if (apps) {
      this.apps = apps instanceof Apps ? apps : new Apps(apps)
      this._fetched(done)
    } else if (this.options.dataSrc) {
      this._fetch(function (data) {
        this.apps = new Apps(data)
        this._fetched(done)
      }.bind(this))
    } else {
      throw new Error('Apps data cannot be retrieved. A dataSrc option is required to fetch it, or provide an Apps collection as third argument')
    }
    return this
  }

  /**
   * Initializes the Hosts collection, and add every available App to them
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  _initHosts () {
    this.hosts = new Hosts()
    this.apps.models.forEach(app => this.hosts.addAppToHosts(app, {silent: true}))
    return this
  }

  /**
   * Creates a HostTopApps view for every Host, and save it as view Host property
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  _initHostsTopAppsViews () {
    this.hosts.models.forEach(host => {
      host.view = new HostTopApps(host, {
        apps: this.options.appsByHost
      })
    })
    return this
  }

  /**
   * Append every HostTopApps view to the content element, and removes the unresolved state
   * @returns {ApdexBoard} - Current ApdexBoard instance
   */
  _appendHostsTopAppsViews () {
    this.hosts.models.forEach(host => {
      this.contentElement.appendChild(host.view.element)
    })
    this.contentElement.classList.remove(this.options.unresolvedStateClassName)
    return this
  }

  /**
   * Creates a GetJSON instance and starts fetching data from dataSrc option url
   * @param {doneCallback} done - The callback with the response
   * @returns {GetJSON} - Current GetJSON instance
   */
  _fetch (done) {
    let request = new GetJSON(this.options.dataSrc)
    request.get(
      function (data) {
        if (typeof done === 'function') done(data)
      },
      function (status) {
        throw new Error(`Apps data cannot be retrieved from ${this.options.dataSrc} (${status})`)
      }.bind(this))
    return request;
  }

  /**
   * Emits the fetched event and executes given callback
   * @param {simpleCallback|doneCallback} done - Executed after firing events
   */
  _fetched (done) {
    this.fetched = true
    this.events.emit('fetched')
    if (typeof done === 'function') done()
  }

  /**
   * Toggle the contentElement view from grid to list, according the eventTarget checked property
   * @param {Event} ev - Event fired from a checkbox input
   */
  _displayToggle (ev) {
    this.contentElement.classList.toggle(this.options.gridViewClassName, ev ? !ev.target.checked : undefined)
  }
}

module.exports = ApdexBoard;


/**
 * Enqueue the dataApi static method on document load
 */
(function (ApdexBoard, window, document) {
  if (document.readyState !== 'loading') {
    ApdexBoard.dataApi(window, document)
  } else {
    document.addEventListener('DOMContentLoaded', function (ev) {
      ApdexBoard.dataApi(window, document)
    })
  }
})(ApdexBoard, window, document)
