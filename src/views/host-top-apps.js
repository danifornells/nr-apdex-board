const Host = require('../models/host')

const defaultOptions = {
  apps: 5,
  tagName: 'DIV',
  classNames: ['o-layout__item'],
  appEventAttribute: 'data-host-top-apps__app-index',
  template: function (data) {
    return `
      <div class="cs-neutral-light c-box">
        <h2 class="m-heading m-heading--small m-heading--single-line">${data.name}</h2>
        <div class="c-list-scored">
          ${data.apps.map((app, i) =>
            `<div class="c-list-scored__item">
              <div class="c-list-scored__item-score">${app.apdex}</div>
              <button class="c-list-scored__item-body" ${app.eventAttribute}="${i}">${app.name}</button>
            </div>`
          ).join('')}
        </div>
      </div>
    `
  }
}

/**
 * A HostTopApps view
 */
class HostTopApps {

  /**
   * Create a new HostTopApps view, including render and events binding
   *
   * @constructor
   *
   * @param {Host} model - The Host model who to retrieve top Apps
   * @param {Object} options - The view options
   * @param {Number} options.apps - The number of apps to show
   * @param {String} options.tagName - The HTMLElement tag used on view element
   * @param {String[]} options.classNames - Class names to add to view element
   * @param {String} options.appEventAttribute - The attribute to identify a
   * shown app and catch view delegated events
   * @param {Function} options.template - The template function that returns the
   * view innerHTML for the given data
   */
  constructor (model, options) {
    if (!model || !(model instanceof Host)) throw new Error('HostTopApps requires a Host instance as first argument')
    this.model = model
    this.options = {}
    Object.keys(defaultOptions).forEach(key => {
      this.options[key] = options ? options[key] || defaultOptions[key] : defaultOptions[key]
    })
    this.clickEvent = this._clickEvent.bind(this)
    this
      .render()
      .unbindEvents()
      .bindEvents()
  }

  /**
   * Renders the view to the View element property with the Host data.
   * If the element is not created yet, it creates a new one.
   *
   * @returns {HostTopApps} The current view
   */
  render () {
    this.appsOnScreen = this.model.getTopApps(this.options.apps)
    if (!this.element) this.element = window.document.createElement(this.options.tagName)
    this.element.classList.add(...this.options.classNames)
    this.element.innerHTML = this.options.template({
      name: this.model.attributes.name,
      apps: this.appsOnScreen.map(app => {
        return {
          apdex: app.attributes.apdex,
          name: app.attributes.name,
          eventAttribute: this.options.appEventAttribute
        }
      })
    })
    return this
  }

  /**
   * Bind the click event to the View element property, and listen for
   * the Host Apps collection update to refresh the view using render.
   * Since is using event delegation for the click event, there's no
   * needing for rebinding events after a render.
   *
   * @returns {HostTopApps} The current view
   */
  bindEvents () {
    this.element.addEventListener('click', this.clickEvent)
    this.model.apps.events.on('updated', this._appsUpdated, this)
    return this
  }

  /**
   * Unbind the events bound with bindEvents
   *
   * @returns {HostTopApps} The current view
   */
  unbindEvents () {
    this.element.removeEventListener('click', this.clickEvent)
    this.model.apps.events.removeListener('updated', this._appsUpdated)
    return this
  }

  /**
   * Caught a click event on any App, alerts with App info
   *
   * @params {Event} ev - The captured event
   */
  _clickEvent (ev) {
    if (!ev.target.hasAttribute(this.options.appEventAttribute)) return
    const clickedApp = this.appsOnScreen[parseInt(ev.target.getAttribute(this.options.appEventAttribute))]
    window.alert(`
      ${clickedApp.attributes.name.toUpperCase()}\n
      Version:\t\t${clickedApp.attributes.version}\r
      Contributors:\t${clickedApp.attributes.contributors.join(', ')}\r
      Hosts:\t\t${clickedApp.getHostNames().join(', ')}
    `)
  }

  _appsUpdated () {
    this.render()
  }
}

module.exports = HostTopApps
