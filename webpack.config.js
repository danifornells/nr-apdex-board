const path = require('path')
const pkg = require('./package.json')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = [
  {
    devtool: 'cheap-module-source-map',
    entry: ['./src/apdex-board.js', './styles/main.scss'],
    output: {
      filename: `${pkg.name}.js`,
      path: path.resolve(__dirname, '_dist')
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'buble-loader'
          }
        }, {
          test: /\.scss$/,
          exclude: /(node_modules)/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader'
              }, {
                loader: 'postcss-loader'
              }, {
                loader: 'sass-loader'
              }
            ]
          })
        }, {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'url-loader',
          query: {
            limit: '10000',
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: pkg.name,
        description: pkg.description,
        author: pkg.author.name,
        template: 'src/index.html',
        inject: false
      }),
      new CopyWebpackPlugin([{ from: './data/host-app-data.json' }]),
      new ExtractTextPlugin({
        filename: 'styles.css',
        allChunks: true
      }),
      new UglifyJSPlugin(),
      new BundleAnalyzerPlugin({
        analyzerMode: 'disabled',
        defaultSizes: 'parsed',
        generateStatsFile: true,
        statsOptions: {chunkModules: true},
        statsFilename: `${pkg.name}.json`,
        logLevel: 'info'
      })
    ]
  },
  {
    entry: './test/browser-tests.js',
    output: {
      filename: 'browser-tests-es5.js',
      path: path.resolve(__dirname, 'test')
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'buble-loader'
          }
        }
      ]
    }
  }
]
