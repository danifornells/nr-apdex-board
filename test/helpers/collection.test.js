const Model = require('../../src/helpers/model')
const Collection = require('../../src/helpers/collection')
const EventEmitter = require('eventemitter3')

class CollectionModel extends Model {
  constructor (attributes) {
    super({
      name: null,
      description: null
    }, attributes)
  }
}

const collectionItems = [
  {
    name: 'Member one',
    description: 'Il buono'
  },
  {
    name: 'Member two',
    description: 'Il brutto'
  },
  {
    name: 'Member three',
    description: 'Il cattivo'
  }
]

describe('helpers > collection', () => {
  describe('> constructor :', () => {
    it('can be instantiated', () => {
      let myCollection = new Collection()
      expect(myCollection).to.be.ok
      expect(myCollection instanceof Collection).to.be.true
    })

    it('stores a model class as model property', () => {
      let myCollection = new Collection(CollectionModel)
      expect(myCollection.Model).to.equal(CollectionModel)
    })

    it('has an array of items as models property', () => {
      let myCollection = new Collection()
      expect(myCollection.models).to.be.a('array')
      expect(myCollection.models.length).to.equal(0)
    })

    it('given items are added to the collection', () => {
      let myCollection = new Collection(CollectionModel, collectionItems)
      expect(myCollection.models.length).to.equal(collectionItems.length)
    })

    it('has an events emitter instance as event property', () => {
      let myCollection = new Collection(CollectionModel, collectionItems)
      expect(myCollection.events instanceof EventEmitter).to.be.true
    })
  })

  describe('> methods', () => {
    describe('> addItem :', () => {
      it('is available', () => {
        let myCollection = new Collection(CollectionModel)
        expect(typeof myCollection.addItem).to.equal('function')
      })

      it('given item is added to collection', () => {
        let myCollection = new Collection(CollectionModel)
        myCollection.addItem(collectionItems[0])
        expect(myCollection.models.length).to.equal(1)
      })

      it('given json item had become a collection model instance', () => {
        let myCollection = new Collection(CollectionModel)
        myCollection.addItem(collectionItems[0])
        expect(myCollection.models[0] instanceof Model).to.be.true
        expect(myCollection.models[0] instanceof CollectionModel).to.be.true
      })

      it('given model item remains same model instance', () => {
        let myCollection = new Collection(CollectionModel),
          myItem = new CollectionModel({
            name: 'Lorem',
            description: 'Ipsum dolor'
          })
        myCollection.addItem(myItem)
        expect(myCollection.models[0] instanceof Model).to.be.true
        expect(myCollection.models[0] instanceof CollectionModel).to.be.true
        expect(myCollection.models[0]).to.equal(myItem)
      })

      it('returns the new item added to collection', () => {
        let myCollection = new Collection(CollectionModel),
          itemAdded = myCollection.addItem(collectionItems[0])
        expect(itemAdded instanceof Model).to.be.true
        expect(itemAdded.attributes.name).to.equal(collectionItems[0].name)
      })

      it('triggers a single updated event', () => {
        let myCollection = new Collection(CollectionModel),
          eventCount = 0
        myCollection.events.on('updated', function () {
          eventCount++
        }, this)
        myCollection.addItem(collectionItems[0])
        expect(eventCount).to.be.equal(1)
      })

      it('does not trigger any updated event if option silent has set to true', () => {
        let myCollection = new Collection(CollectionModel),
          eventCount = 0
        myCollection.events.on('updated', function () {
          eventCount++
        }, this)
        myCollection.addItem(collectionItems[0], {silent: true})
        expect(eventCount).to.be.equal(0)
      })
    })

    describe('> addItems :', () => {
      it('is available', () => {
        let myCollection = new Collection(CollectionModel)
        expect(typeof myCollection.addItems).to.equal('function')
      })

      it('given items are added to an empty collection', () => {
        let myCollection = new Collection(CollectionModel)
        myCollection.addItems(collectionItems)
        expect(myCollection.models.length).to.equal(collectionItems.length)
      })

      it('given items are added to a non empty collection', () => {
        let myCollection = new Collection(CollectionModel, collectionItems)
        myCollection.addItems(collectionItems)
        expect(myCollection.models.length).to.equal(collectionItems.length * 2)
      })

      it('triggers a single updated event', () => {
        let myCollection = new Collection(CollectionModel, collectionItems),
          eventCount = 0
        myCollection.events.on('updated', function () {
          eventCount++
        }, this)
        myCollection.addItems(collectionItems)
        expect(eventCount).to.be.equal(1)
      })

      it('does not trigger any updated event if option silent has set to true', () => {
        let myCollection = new Collection(CollectionModel, collectionItems),
          eventCount = 0
        myCollection.events.on('updated', function () {
          eventCount++
        }, this)
        myCollection.addItems(collectionItems, {silent: true})
        expect(eventCount).to.be.equal(0)
      })
    })

    describe('> findWhere :', () => {
      it('is available', () => {
        let myCollection = new Collection(CollectionModel)
        expect(typeof myCollection.findWhere).to.equal('function')
      })

      it('given an attribute and value returns the first item match', () => {
        let myCollection = new Collection(CollectionModel, collectionItems),
          searchResult = myCollection.findWhere('name', 'Member two')
        expect(searchResult).to.be.ok
        expect(searchResult instanceof CollectionModel).to.be.true
        expect(searchResult).to.equal(myCollection.models[1])
      })

      it('return undefined if no results are found', () => {
        let myCollection = new Collection(CollectionModel, collectionItems),
          searchResult = myCollection.findWhere('name', 'Member four')
        expect(searchResult).not.to.be.ok
      })
    })
  })
})
