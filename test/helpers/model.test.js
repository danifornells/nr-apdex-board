const Model = require('../../src/helpers/model')

const modelDefaultAttributes = {
  name: null,
  description: null
}

describe('helpers > model', () => {
  describe('> constructor :', () => {
    it('can be instantiated', () => {
      let myModel = new Model(modelDefaultAttributes, {})
      expect(myModel).to.be.ok
      expect(myModel instanceof Model).to.be.true
    })

    it('uses default attributes', () => {
      let myModel = new Model(modelDefaultAttributes, {})
      expect(myModel.attributes).to.be.a('object')
      Object.keys(modelDefaultAttributes).forEach(key => {
        expect(myModel.attributes[key]).to.equal(modelDefaultAttributes[key])
      })
    })

    it('current attributes overrides the default ones', () => {
      const customAttributes = {
        name: 'New Relic',
        description: 'Code challenge'
      }
      let myModel = new Model(modelDefaultAttributes, customAttributes)
      expect(myModel.attributes).to.be.a('object')
      Object.keys(customAttributes).forEach(key => {
        expect(myModel.attributes[key]).not.to.equal(modelDefaultAttributes[key])
        expect(myModel.attributes[key]).to.equal(customAttributes[key])
      })
    })
  })
})
