
require('./helpers/model.test')
require('./helpers/collection.test')

require('./models/app.test')
require('./collections/apps.test')
require('./models/host.test')
require('./collections/hosts.test')

require('./views/host-top-apps.test')

require('./apdex-board.test')
