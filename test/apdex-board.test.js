if (isBrowser) {
  const Apps = require('../src/collections/apps')
  const Hosts = require('../src/collections/hosts')
  const HostTopApps = require('../src/views/host-top-apps')
  const ApdexBoard = require('../src/apdex-board')

  const appsSample = require('./collections/apps-sample.json')
  const appsSampleMedium = require('./collections/apps-sample-medium.json')

  describe('appdexBoard app', () => {
    describe('> constructor :', () => {
      it('can be instantiated with an HTML element', () => {
        const myDiv = document.createElement('DIV'),
          myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
        expect(myApdexBoard).to.be.ok
        expect(myApdexBoard instanceof ApdexBoard).to.be.true
      })

      it('can not be instantiated without an HTML element', () => {
        expect(function () { new ApdexBoard() }).to.throw('ApdexBoard requires a HTMLElement as first argument')
      })

      it('uses default options', () => {
        const myDiv = document.createElement('DIV'),
          myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
        expect(myApdexBoard.options.appsByHost).to.equal(5)
        expect(myApdexBoard.options.dataSrc).to.equal(null)
      })

      it('given options overrides the default ones', () => {
        const myDiv = document.createElement('DIV'),
          myApdexBoard = new ApdexBoard(myDiv, {
            appsByHost: 10,
            dataSrc: 'myDataSrc.json'
          }, appsSample)
        expect(myApdexBoard.options.appsByHost).to.equal(10)
        expect(myApdexBoard.options.dataSrc).to.equal('myDataSrc.json')
      })

      describe('> when apps are given as argument :', () => {
        it('creates and exposes an app collection as apps property', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(myApdexBoard.apps).to.be.ok
          expect(myApdexBoard.apps instanceof Apps).to.be.true
          expect(myApdexBoard.apps.models.length).to.be.equal(appsSample.length)
        })

        it('sets fetched property to true', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(myApdexBoard.fetched).to.be.true
        })
      })

      describe('> when apps are missing as argument :', () => {
        it('throws an error if there\'s no dataSrc available', () => {
          const myDiv = document.createElement('DIV')
          expect(function () { new ApdexBoard(myDiv) }).to.throw('Apps data cannot be retrieved. A dataSrc option is required to fetch it, or provide an Apps collection as third argument')
        })

        it('fetch asynchronous data from dataSrc option', (done) => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {
              dataSrc: '_dist/host-app-data.json'
            })
          expect(myApdexBoard.fetched).to.be.false
          const resolver = function () {
            expect(myApdexBoard.fetched).to.be.true
            expect(myApdexBoard.apps.models.length).to.be.equal(10000)
            myApdexBoard.events.removeListener('fetched', resolver)
            done()
          }
          myApdexBoard.events.on('fetched', resolver, this)
          let resolverTimeout = setTimeout(resolver, 1000)
        })
      })

      describe('> hosts :', () => {
        it('creates and exposes a hosts collection as hosts property', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(myApdexBoard.hosts).to.be.ok
          expect(myApdexBoard.hosts instanceof Hosts).to.be.true
        })

        it('every host created has a HostTopApps view as view property', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          myApdexBoard.hosts.models.forEach(host => {
            expect(host.view).to.be.ok
            expect(host.view instanceof HostTopApps).to.be.true
          })
        })

        it('every host view had been appended to app element', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          myApdexBoard.hosts.models.forEach(host => {
            expect(myDiv.contains(host.view.element)).to.be.true
            expect(myApdexBoard.element.contains(host.view.element)).to.be.true
          })
        })
      })

      describe('> jsApi :', () => {
        it('has been exposed on the given HTML element as apdexBoard property', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(myDiv.apdexBoard).to.be.ok
        })

        it('contains apps and hosts collections as properties', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(myDiv.apdexBoard.apps).to.be.equal(myApdexBoard.apps)
          expect(myDiv.apdexBoard.hosts).to.be.equal(myApdexBoard.hosts)
        })
      })
    })

    describe('> methods', () => {
      describe('> getTopAppsByHost :', () => {
        it('is available', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(typeof myApdexBoard.getTopAppsByHost).to.equal('function')
        })

        it('returns the top 25 apps by apdex for a given host name', () => {
          const appsSample = appsSampleMedium.map(app => {
            app.host.push('myhost.newrelic.com')
            return app
          })
          let bestApp = {apdex: 0}
          appsSample.forEach(app => {
            bestApp = app.apdex > bestApp.apdex ? app : bestApp
          })
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          const myApps = myApdexBoard.getTopAppsByHost('myhost.newrelic.com')
          expect(myApps.length).to.equal(25)
          expect(myApps[0].attributes.name).to.equal(bestApp.name)
        })
      })

      describe('> addAppToHosts :', () => {
        it('is available', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(typeof myApdexBoard.addAppToHosts).to.equal('function')
        })

        it('it adds a new app to the main apps collection using addItem method', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample),
            spy = sinon.spy(myApdexBoard.apps, 'addItem').reset(),
            myNewApp = {
              name: 'myNewApp', hosts: ['unknown.newrelic.com']
            }
          expect(myApdexBoard.apps.models.length).to.equal(appsSample.length)
          myApdexBoard.addAppToHosts(myNewApp)
          expect(myApdexBoard.apps.models.length).to.equal(appsSample.length + 1)
          expect(spy).to.have.been.callCount(1)
        })

        it('the app is added to hosts collection using addAppToHosts method', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample),
            spy = sinon.spy(myApdexBoard.hosts, 'addAppToHosts').reset(),
            hostTargetName = appsSample[0].host[0],
            hostTargetModel = myApdexBoard.hosts.getHostByName(hostTargetName),
            hostTargetLength = hostTargetModel.apps.models.length,
            myNewApp = {
              name: 'myNewApp', host: [hostTargetName]
            }
          myApdexBoard.addAppToHosts(myNewApp)
          expect(hostTargetModel.apps.models.length).to.equal(hostTargetLength + 1)
          expect(spy).to.have.been.callCount(1)
        })
      })

      describe('> removeAppFromHosts :', () => {
        it('is available', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample)
          expect(typeof myApdexBoard.removeAppFromHosts).to.equal('function')
        })

        it('it calls removeApp method on every host', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample),
            spies = myApdexBoard.hosts.models.map(host => {
              return sinon.spy(host, 'removeApp').reset()
            })
          myApdexBoard.removeAppFromHosts('myApp')
          spies.forEach(spy => {
            expect(spy).to.have.been.callCount(1)
            expect(spy).to.have.been.calledWith('myApp')
          })
        })

        it('it calls render method only on hosts views where the app has been removed', () => {
          const myDiv = document.createElement('DIV'),
            myApdexBoard = new ApdexBoard(myDiv, {}, appsSample),
            appToRemove = myApdexBoard.apps.models[0],
            hostsWithApp = appToRemove.getHostNames().map(hostName => {
              return myApdexBoard.hosts.getHostByName(hostName)
            }),
            hostsWithoutApp = myApdexBoard.hosts.models.filter(host => {
              return hostsWithApp.indexOf(host) < 0
            }),
            spiesWithApp = hostsWithApp.map(host => {
              return sinon.spy(host.view, 'render').reset()
            }),
            spiesWithoutApp = hostsWithoutApp.map(host => {
              return sinon.spy(host.view, 'render').reset()
            })
          myApdexBoard.removeAppFromHosts(appToRemove)
          spiesWithApp.forEach(spy => {
            expect(spy).to.have.been.callCount(1)
          })
          spiesWithoutApp.forEach(spy => {
            expect(spy).to.have.been.callCount(0)
          })
        })
      })
    })
  })
}
