if (isBrowser) {
  const Host = require('../../src/models/host')
  const HostTopApps = require('../../src/views/host-top-apps')
  const hostSample = {name: 'myhost.com'}

  describe('views > hostTopApps', () => {
    describe('> constructor :', () => {
      it('can be instantiated with a Host model', () => {
        const myHost = new Host(hostSample),
          myHostTopApps = new HostTopApps(myHost)
        expect(myHostTopApps).to.be.ok
        expect(myHostTopApps instanceof HostTopApps).to.be.true
      })

      it('can not be instantiated without a Host model', () => {
        expect(function () { new HostTopApps() }).to.throw('HostTopApps requires a Host instance as first argument')
      })

      it('uses default options', () => {
        const myHost = new Host(hostSample),
          myHostTopApps = new HostTopApps(myHost)
        expect(myHostTopApps.options.apps).to.equal(5)
        expect(myHostTopApps.options.tagName).to.equal('DIV')
      })

      it('given options overrides the default ones', () => {
        const myHost = new Host(hostSample),
          myHostTopApps = new HostTopApps(myHost, {
            apps: 10,
            classNames: ['customized-name']
          })
        expect(myHostTopApps.options.apps).to.equal(10)
        expect(myHostTopApps.options.classNames).to.deep.equal(['customized-name'])
      })

      it('stores the Host model to the property model', () => {
        const myHost = new Host(hostSample),
          myHostTopApps = new HostTopApps(myHost)
        expect(myHostTopApps.model).to.equal(myHost)
      })

      it('calls the render method and exposes the rendered HTML as element property', () => {
        const myHost = new Host(hostSample),
          myHostTopApps = new HostTopApps(myHost)
        expect(myHostTopApps.element instanceof HTMLElement).to.be.true
      })
    })

    describe('> methods', () => {
      describe('> render :', () => {
        it('is available', () => {
          const myHost = new Host(hostSample),
            myHostTopApps = new HostTopApps(myHost)
          expect(typeof myHostTopApps.render).to.equal('function')
        })

        it('it is called every time the model app collection is updated', () => {
          const myHost = new Host(hostSample),
            myHostTopApps = new HostTopApps(myHost),
            spy = sinon.spy(myHostTopApps, 'render').reset()
          myHost.addApp({name: 'myApp', hosts: [hostSample.name]})
          expect(spy).to.have.been.callCount(1)
        })
      })
    })
  })
}
