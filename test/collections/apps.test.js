const Collection = require('../../src/helpers/collection')
const App = require('../../src/models/app')
const Apps = require('../../src/collections/apps')

const appsSample = require('./apps-sample.json')

describe('collections > apps', () => {
  describe('> constructor :', () => {
    it('can be instantiated', () => {
      let myApps = new Apps()
      expect(myApps).to.be.ok
      expect(myApps instanceof Apps).to.be.true
      expect(myApps instanceof Collection).to.be.true
    })

    it('has App as the collection model', () => {
      let myApps = new Apps()
      expect(myApps.Model).to.equal(App)
    })

    it('app items data can be added as an argument', () => {
      let myApps = new Apps(appsSample)
      expect(myApps.models.length).to.equal(appsSample.length)
      expect(myApps.models[0] instanceof App).to.be.true
    })
  })

  describe('> methods', () => {
    describe('> removeApp :', () => {
      it('is available', () => {
        let myApps = new Apps()
        expect(typeof myApps.removeApp).to.equal('function')
      })

      describe('> given an App model as argument :', () => {
        describe('> if the App was in collection :', () => {
          it('it is removed from collection', () => {
            let myApps = new Apps(appsSample)
            expect(myApps.models.length).to.equal(appsSample.length)
            myApps.removeApp(myApps.models[0])
            expect(myApps.models.length).to.equal(appsSample.length - 1)
          })

          it('it returns the removed App model', () => {
            let myApps = new Apps(appsSample),
              appToRemove = myApps.models[0],
              removedApp = myApps.removeApp(appToRemove)
            expect(removedApp).to.equal(appToRemove)
          })

          it('it triggers an update event', () => {
            let myApps = new Apps(appsSample),
              appToRemove = myApps.models[0],
              eventCount = 0
            myApps.events.on('updated', function () {
              eventCount++
            }, this)
            myApps.removeApp(appToRemove)
            expect(eventCount).to.be.equal(1)
          })

          it('does not trigger any updated event if option silent has set to true', () => {
            let myApps = new Apps(appsSample),
              appToRemove = myApps.models[0],
              eventCount = 0
            myApps.events.on('updated', function () {
              eventCount++
            }, this)
            myApps.removeApp(appToRemove, {silent: true})
            expect(eventCount).to.be.equal(0)
          })
        })

        describe('> if the App was NOT in collection :', () => {
          it('it returns undefined', () => {
            let myApps = new Apps(appsSample),
              appToRemove = new App({name: 'myApp'}),
              removedApp = myApps.removeApp(appToRemove)
            expect(removedApp).to.be.not.ok
          })

          it('does not trigger any updated event', () => {
            let myApps = new Apps(appsSample),
              appToRemove = new App({name: 'myApp'}),
              eventCount = 0
            myApps.events.on('updated', function () {
              eventCount++
            }, this)
            myApps.removeApp(appToRemove)
            expect(eventCount).to.be.equal(0)
          })
        })
      })

      describe('> given an app data as argument :', () => {
        describe('> if an app with same name was in collection :', () => {
          it('it is removed from collection', () => {
            let myApps = new Apps(appsSample),
              appToRemove = {name: appsSample[0].name}
            expect(myApps.models.length).to.equal(appsSample.length)
            myApps.removeApp(appToRemove)
            expect(myApps.models.length).to.equal(appsSample.length - 1)
          })

          it('it returns the removed App model', () => {
            let myApps = new Apps(appsSample),
              appToRemove = {name: appsSample[0].name},
              appExpectedToBeRemoved = myApps.models[0],
              removedApp = myApps.removeApp(appToRemove)
            expect(removedApp).to.equal(appExpectedToBeRemoved)
          })

          it('it triggers an update event', () => {
            let myApps = new Apps(appsSample),
              appToRemove = {name: appsSample[0].name},
              eventCount = 0
            myApps.events.on('updated', function () {
              eventCount++
            }, this)
            myApps.removeApp(appToRemove)
            expect(eventCount).to.be.equal(1)
          })
        })

        describe('> if there was no other app with the same name in collection :', () => {
          it('it returns undefined', () => {
            let myApps = new Apps(appsSample),
              appToRemove = {name: 'myApp'},
              removedApp = myApps.removeApp(appToRemove)
            expect(removedApp).to.be.not.ok
          })

          it('does not trigger any updated event', () => {
            let myApps = new Apps(appsSample),
              appToRemove = {name: 'myApp'},
              eventCount = 0
            myApps.events.on('updated', function () {
              eventCount++
            }, this)
            myApps.removeApp(appToRemove)
            expect(eventCount).to.be.equal(0)
          })
        })
      })

      describe('> given an app name as argument :', () => {
        describe('> if an app with same name was in collection :', () => {
          it('it is removed from collection', () => {
            let myApps = new Apps(appsSample),
              appToRemove = appsSample[0].name
            expect(myApps.models.length).to.equal(appsSample.length)
            myApps.removeApp(appToRemove)
            expect(myApps.models.length).to.equal(appsSample.length - 1)
          })

          it('it returns the removed App model', () => {
            let myApps = new Apps(appsSample),
              appToRemove = appsSample[0].name,
              appExpectedToBeRemoved = myApps.models[0],
              removedApp = myApps.removeApp(appToRemove)
            expect(removedApp).to.equal(appExpectedToBeRemoved)
          })

          it('it triggers an update event', () => {
            let myApps = new Apps(appsSample),
              appToRemove = appsSample[0].name,
              eventCount = 0
            myApps.events.on('updated', function () {
              eventCount++
            }, this)
            myApps.removeApp(appToRemove)
            expect(eventCount).to.be.equal(1)
          })
        })

        describe('> if there was no other app with the same name in collection :', () => {
          it('it returns undefined', () => {
            let myApps = new Apps(appsSample),
              appToRemove = 'myApp',
              removedApp = myApps.removeApp(appToRemove)
            expect(removedApp).to.be.not.ok
          })

          it('does not trigger any updated event', () => {
            let myApps = new Apps(appsSample),
              appToRemove = 'myApp',
              eventCount = 0
            myApps.events.on('updated', function () {
              eventCount++
            }, this)
            myApps.removeApp(appToRemove)
            expect(eventCount).to.be.equal(0)
          })
        })
      })
    })
  })
})
