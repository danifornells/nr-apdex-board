const Collection = require('../../src/helpers/collection')
const App = require('../../src/models/app')
const Apps = require('../../src/collections/apps')
const Host = require('../../src/models/host')
const Hosts = require('../../src/collections/hosts')

const appSample = require('../models/app-sample.json')
const appsSample = require('./apps-sample.json')
const hostsSample = appSample.host.map(hostName => {
  return {name: hostName}
})

describe('collections > hosts', () => {
  describe('> constructor :', () => {
    it('can be instantiated', () => {
      let myHosts = new Hosts()
      expect(myHosts).to.be.ok
      expect(myHosts instanceof Hosts).to.be.true
      expect(myHosts instanceof Collection).to.be.true
    })

    it('has Host as the collection model', () => {
      let myHosts = new Hosts()
      expect(myHosts.Model).to.equal(Host)
    })
  })

  describe('> methods', () => {
    describe('> addHostByName :', () => {
      it('is available', () => {
        let myHosts = new Hosts()
        expect(typeof myHosts.addHostByName).to.equal('function')
      })

      it('adds a new host to the collection with given name using addItem collection method', () => {
        let myHosts = new Hosts(),
          spy = sinon.spy(myHosts, 'addItem').reset()
        expect(myHosts.models.length).to.equal(0)
        myHosts.addHostByName(appSample.host[0])
        expect(myHosts.models.length).to.equal(1)
        expect(myHosts.models[0].attributes.name).to.equal(appSample.host[0])
        expect(spy).to.have.been.callCount(1)
      })

      it('returns the new created host', () => {
        let myHosts = new Hosts(),
          newHost = myHosts.addHostByName(appSample.host[0])
        expect(newHost instanceof Host).to.be.true
        expect(newHost.attributes.name).to.be.equal(appSample.host[0])
      })
    })

    describe('> getHostByName :', () => {
      it('is available', () => {
        let myHosts = new Hosts()
        expect(typeof myHosts.getHostByName).to.equal('function')
      })

      it('returns the host if exists', () => {
        let myHosts = new Hosts([{name: appSample.host[0]}]),
          searchResult = myHosts.getHostByName(appSample.host[0])
        expect(searchResult instanceof Host).to.be.true
        expect(searchResult).to.be.equal(myHosts.models[0])
      })

      it('return undefined if no results are found', () => {
        let myHosts = new Hosts([{name: appSample.host[0]}]),
          searchResult = myHosts.getHostByName('Lorem ipsum')
        expect(searchResult).not.to.be.ok
      })
    })

    describe('> addAppToHost :', () => {
      it('is available', () => {
        let myHosts = new Hosts()
        expect(typeof myHosts.addAppToHost).to.equal('function')
      })

      it('given app and valid host name, the app is added to host apps collection', () => {
        let myHosts = new Hosts(hostsSample),
          myApp = new App(appSample),
          curentHost = myHosts.getHostByName(appSample.host[1])
        expect(curentHost).to.be.ok
        expect(curentHost.apps.models.length).to.equal(0)
        myHosts.addAppToHost(myApp, curentHost.attributes.name)
        expect(curentHost.apps.models.length).to.equal(1)
        expect(curentHost.apps.models[0]).to.equal(myApp)
      })

      it('given app and new host name, the host is created and app is added', () => {
        let myHosts = new Hosts(),
          myApp = new App(appSample),
          myHostName = appSample.host[1],
          curentHost = myHosts.getHostByName(myHostName)
        expect(curentHost).not.to.be.ok
        myHosts.addAppToHost(myApp, myHostName)
        curentHost = myHosts.getHostByName(myHostName)
        expect(curentHost).to.be.ok
        expect(curentHost instanceof Host).to.be.true
        expect(curentHost.apps.models.length).to.equal(1)
        expect(curentHost.apps.models[0]).to.equal(myApp)
      })

      it('returns itself', () => {
        let myHosts = new Hosts(hostsSample),
          myApp = new App(appSample),
          curentHost = myHosts.findWhere('name', appSample.host[1])
        expect(myHosts.addAppToHost(myApp, curentHost.attributes.name)).to.equal(myHosts)
      })
    })

    describe('> addAppToHosts :', () => {
      it('is available', () => {
        let myHosts = new Hosts()
        expect(typeof myHosts.addAppToHosts).to.equal('function')
      })

      it('should call addAppToHost for each app host', () => {
        let myHosts = new Hosts(),
          myApp = new App(appSample),
          spy = sinon.spy(myHosts, 'addAppToHost').reset()
        myHosts.addAppToHosts(myApp)
        expect(spy).to.have.been.callCount(myApp.getHostNames().length)
      })

      it('given an app, the app is added to each host apps collection', () => {
        let myHosts = new Hosts(hostsSample),
          myApp = new App(appSample)
        myHosts.models.forEach(host => {
          expect(host.apps.models.length).to.equal(0)
        })
        myHosts.addAppToHosts(myApp)
        myApp.getHostNames().forEach(hostName => {
          let curentHost = myHosts.findWhere('name', hostName)
          expect(curentHost.apps.models.length).to.equal(1)
          expect(curentHost.apps.models[0]).to.equal(myApp)
        })
      })

      it('returns itself', () => {
        let myHosts = new Hosts(hostsSample),
          myApp = new App(appSample)
        expect(myHosts.addAppToHosts(myApp)).to.equal(myHosts)
      })
    })
  })
})
