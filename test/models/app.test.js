const Model = require('../../src/helpers/model')
const App = require('../../src/models/app')

const appDefaultAttributes = require('./app-defaults.json')
const appSample = require('./app-sample.json')

describe('models > app', () => {
  describe('> constructor :', () => {
    it('can be instantiated', () => {
      let myApp = new App({})
      expect(myApp).to.be.ok
      expect(myApp instanceof Model).to.be.true
      expect(myApp instanceof App).to.be.true
    })

    it('uses default attributes', () => {
      let myApp = new App({})
      expect(myApp.attributes).to.be.a('object')
      expect(myApp.attributes).to.deep.equal(appDefaultAttributes)
    })

    it('current attributes overrides the default ones', () => {
      let myApp = new App(appSample)
      expect(myApp.attributes).to.be.a('object')
      Object.keys(appSample).forEach(key => {
        expect(myApp.attributes[key]).not.to.equal(appDefaultAttributes[key])
        expect(myApp.attributes[key]).to.equal(appSample[key])
      })
    })
  })

  describe('> methods', () => {
    describe('> getHostNames :', () => {
      it('is available', () => {
        let myApp = new App({})
        expect(typeof myApp.getHostNames).to.equal('function')
      })

      it('returns an empty array if there are no hosts', () => {
        let myApp = new App({}),
          myHostNames = myApp.getHostNames()
        expect(myHostNames).to.be.a('array')
      })

      it('returns the given host attribute', () => {
        let myApp = new App(appSample),
          myHostNames = myApp.getHostNames()
        expect(myHostNames).to.equal(appSample.host)
      })
    })
  })
})
