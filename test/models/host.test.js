const Model = require('../../src/helpers/model')
const Collection = require('../../src/helpers/collection')
const App = require('../../src/models/app')
const Apps = require('../../src/collections/apps')
const Host = require('../../src/models/host')

const hostDefaultAttributes = {name: null}
const hostSample = {name: 'myhost.com'}
const appSample = require('./app-sample.json')
const appsSample = require('../collections/apps-sample.json')

describe('models > host', () => {
  describe('> constructor :', () => {
    it('can be instantiated', () => {
      let myHost = new Host({})
      expect(myHost).to.be.ok
      expect(myHost instanceof Host).to.be.true
      expect(myHost instanceof Model).to.be.true
    })

    it('uses default attributes', () => {
      let myHost = new Host({})
      expect(myHost.attributes).to.be.a('object')
      expect(myHost.attributes).to.deep.equal(hostDefaultAttributes)
    })

    it('current attributes overrides the default ones', () => {
      let myHost = new Host(hostSample)
      expect(myHost.attributes).to.be.a('object')
      Object.keys(hostSample).forEach(key => {
        expect(myHost.attributes[key]).not.to.equal(hostDefaultAttributes[key])
        expect(myHost.attributes[key]).to.equal(hostSample[key])
      })
    })

    it('creates an empty Apps collection on apps property', () => {
      let myHost = new Host({})
      expect(myHost.apps).to.be.ok
      expect(myHost.apps instanceof Apps).to.be.true
      expect(myHost.apps instanceof Collection).to.be.true
      expect(myHost.apps.models.length).to.equal(0)
    })
  })

  describe('> methods', () => {
    describe('> addApp :', () => {
      it('is available', () => {
        let myHost = new Host({})
        expect(typeof myHost.addApp).to.equal('function')
      })

      describe('once invoked :', () => {
        it('uses apps collection addItem method', () => {
          let myHost = new Host({}),
            myApp = new App(appSample),
            spy = sinon.spy(myHost.apps, 'addItem').reset()
          myHost.addApp(myApp)
          expect(spy).to.have.been.callCount(1)
        })

        it('the given item should be in the collection', () => {
          let myHost = new Host({}),
            myApp = new App(appSample)
          myHost.addApp(myApp)
          expect(myHost.apps.models.length).to.equal(1)
          expect(myHost.apps.models[0]).to.equal(myApp)
        })
      })
    })

    describe('> removeApp :', () => {
      it('uses apps collection removeApp method with same argument', () => {
        let myHost = new Host({})
        spy = sinon.spy(myHost.apps, 'removeApp').reset()
        myHost.removeApp('myApp')
        expect(spy).to.have.been.callCount(1)
        expect(spy).to.have.been.calledWith('myApp')
      })
    })

    describe('> getTopApps :', () => {
      it('is available', () => {
        let myHost = new Host({})
        expect(typeof myHost.getTopApps).to.equal('function')
      })

      it('returns the given amount of apps sorted by apdex', () => {
        let myHost = new Host({}),
          myApps = new Apps(appsSample)
        expect(myHost.apps.models.length).to.equal(0)
        myApps.models.forEach(app => myHost.addApp(app))
        expect(myHost.apps.models.length).to.equal(appsSample.length)
        const myTopApps = myHost.getTopApps(2)
        expect(myTopApps.length).to.equal(2)
        expect(myTopApps[0]).to.equal(myApps.models[1])
        expect(myTopApps[1]).to.equal(myApps.models[0])
      })
    })
  })
})
