// Export modules to global scope as necessary (only for testing)
if (typeof process !== 'undefined') {
  // We are in node. Require modules.
  chai = require('chai')
  expect = chai.expect
  sinon = require('sinon')
  sinonChai = require('sinon-chai')
  chai.use(sinonChai)
  isBrowser = false
} else {
  // We are in the browser. Set up variables like above using served js files.
  expect = chai.expect
  // num and sinon already exported globally in the browser.
  isBrowser = true
}
