/* -------------------------------------------------------------------------
 * LAYOUT
 *
 * Grid-like layout system.
 *
 * The layout object provides us with a column-style layout system. This file
 * contains the basic structural elements, but classes should be complemented
 * with width utilities, for example:
 *
 *   <div class="o-layout">
 *     <div class="o-layout__item  u-1/2">
 *     </div>
 *     <div class="o-layout__item  u-1/2">
 *     </div>
 *   </div>
 *
 * The above will create a two-column structure in which each column will
 * fluidly fill half of the width of the parent. We can have more complex
 * systems:
 *
 *   <div class="o-layout">
 *     <div class="o-layout__item  u-1/1  u-1/3@md">
 *     </div>
 *     <div class="o-layout__item  u-1/2  u-1/3@md">
 *     </div>
 *     <div class="o-layout__item  u-1/2  u-1/3@md">
 *     </div>
 *   </div>
 *
 * The above will create a system in which the first item will be 100% width
 * until we enter our medium breakpoint, when it will become 33.333% width. The
 * second and third items will be 50% of their parent, until they also become
 * 33.333% width at the medium breakpoint.
 *
 * We can also manipulate entire layout systems by adding a series of modifiers
 * to the `.o-layout` block. For example:
 *
 *   <div class="o-layout  o-layout--reverse">
 *
 * This will reverse the displayed order of the system so that it runs in the
 * opposite order to our source, effectively flipping the system over.
 *
 *   <div class="o-layout  o-layout--[right|center]">
 *
 * This will cause the system to fill up from either the centre or the right
 * hand side. Default behaviour is to fill up the layout system from the left.
 *
 * There are plenty more options available to us: explore them below.
 *
 *
 * By default we use the `font-size: 0;` trick to remove whitespace between
 * items. Set this to true in order to use a markup-based strategy like
 * commenting out whitespace or minifying HTML.
 *
 *
 * Params:
 * SPACE .............................. Horizontal separation between layout__items
 *
 */


// Object variables
// --------------------------------------------------

@import "vars";


// Object as a mixin
// --------------------------------------------------

@mixin o-layout($_space-value: $o-layout__space) {
  display: block;
  margin: 0;
  margin-left: -1 * s-core-px-to-rem($_space-value);
  margin-top: -1 * s-core-px-to-rem($_space-value);
  padding: 0;
  list-style: none;
  font-size: 0;
}

@mixin o-layout__item($_space-value: $o-layout__space) {
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
  width: 100%;
  padding-left: s-core-px-to-rem($_space-value);
  padding-top: s-core-px-to-rem($_space-value);
  font-size: s-core-px-to-rem($s-core__font-size);
}


// Object selector output
// --------------------------------------------------

@if ($o-layout--enabled) {
  .o-layout {
    @include o-layout();
  }

  .o-layout__item {
    @include o-layout__item();
  }
}


// Space modifiers
// --------------------------------------------------

@mixin o-layout__mod-space($_space-name: none, $_space-value: 0) {
  .o-layout--space-#{$_space-name} {
    margin-left: -1 * s-core-px-to-rem($_space-value);
    margin-top: -1 * s-core-px-to-rem($_space-value);

    > .o-layout__item {
      padding-left: s-core-px-to-rem($_space-value);
      padding-top: s-core-px-to-rem($_space-value);
    }
  }
}

@if ($o-layout--enabled and $o-layout__mod-spaces--enabled) {
  @each $_space-name, $_space-value in $o-layout__mod-spaces {
    @include o-layout__mod-space($_space-name, $_space-value);
  }
}


// Vertical alignment modifiers
// --------------------------------------------------

@mixin o-layout__mod-alignment-v($_vertical-alignment: top) {
  .o-layout--#{$_vertical-alignment} {
    > .o-layout__item {
      vertical-align: $_vertical-alignment;
    }
  }
}

@if ($o-layout--enabled and $o-layout__mod-alignments-v--enabled) {
  @each $_vertical-alignment in $o-layout__mod-alignments-v {
    @include o-layout__mod-alignment-v($_vertical-alignment);
  }
}


// Horizontal alignment modifiers
// --------------------------------------------------

@mixin o-layout__mod-alignment-h($_alignment: left) {
  .o-layout--#{$_alignment} {
    text-align: $_alignment;

    > .o-layout__item {
      text-align: left;
    }
  }
}

@if ($o-layout--enabled and $o-layout__mod-alignments-h--enabled) {
  @each $_alignment in $o-layout__mod-alignments-h {
    @include o-layout__mod-alignment-h($_alignment);
  }
}


// Reverse modifier
// --------------------------------------------------

@mixin o-layout--reverse() {
  direction: rtl;
}

@mixin o-layout__item--reverse() {
  direction: ltr;
  text-align: left;
}

@if ($o-layout--enabled and $o-layout__mod-reverse--enabled) {
  .o-layout--reverse {
    @include o-layout--reverse();

    > .o-layout__item {
      @include o-layout__item--reverse();
    }
  }

}


// Stretch modifier
// --------------------------------------------------

@mixin o-layout--stretch() {
  display: flex;
  flex-wrap: wrap;
}

@mixin o-layout__item--stretch() {
  display: flex;
}

@mixin o-layout__item--stretch-center() {
  justify-content: center;
}

@mixin o-layout__item--stretch-right() {
  justify-content: flex-end;
}

@if ($o-layout--enabled and $o-layout__mod-stretch--enabled) {
  .o-layout--stretch {
    @include o-layout--stretch();

    > .o-layout__item {
      @include o-layout__item--stretch();

      @if ($o-layout__mod-alignments-h--enabled) {
        &.o-layout--center {
          @include o-layout__item--stretch-center();
        }

        &.o-layout--right {
          @include o-layout__item--stretch-right();
        }
      }
    }
  }

}


// Items auto modifier (sets a predefined with pattern to items)
// --------------------------------------------------

@if ($o-layout--enabled and $o-layout__mod-items-auto--enabled) {

  @each $_size_name, $_columns_map in $o-layout__mod-items-auto {

    .o-layout--items-#{$_size_name} {

      @each $_bp-name, $_columns_number in $_columns_map {
        @if ($_bp-name != "xxs") {

          @include t-mq($from: $_bp-name) {

            @if ($_columns_number != 1) {

              .o-layout__item {
                width: (1 / $_columns_number) * 100% !important;
              }

            }
          }
        }
      }
    }
  }
}



// Unset as mixin
// --------------------------------------------------

@mixin o-layout--unset() {
  display: inherit;
  margin: inherit;
  padding-left: inherit;
  padding-top: inherit;
  list-style: inherit;
  font-size: inherit;
}

@mixin o-layout__item--unset() {
  box-sizing: inherit;
  display: inherit;
  vertical-align: inherit;
  width: auto;
  padding-left: inherit;
  padding-top: inherit;
  font-size: inherit;
}

