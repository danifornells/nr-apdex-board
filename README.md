# nr-apdex-board

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[ ![Codeship Status for danifornells/nr-apdex-board](https://app.codeship.com/projects/9d4759b0-790c-0135-d11a-1af66121fe98/status?branch=master)](https://app.codeship.com/projects/244908)

New Relic Apdex Board - Code challenge


### Getting started

- Install dependencies by `npm i` or `yarn` 
- Build for testing and distribution by `npm run build` or `yarn build`
- Run tests on both Node and Browser environments by `npm run test` or `yarn test`

[I wrote also some **JS documentation** you can read here](DOCS.md)


### About JS approach and structure

Once analyzed the exercise and JSON data structure, I chose a data driven organization of the app, so I made some tiny abstract classes for Models and Collections, like [Backbone](http://backbonejs.org/) well proven in the past, but **without including any framework**, just the things needed in plain ES2015 JS. For the views synchronization I used events, the only tiny library included: [EventEmitter3](https://github.com/primus/eventemitter3).

For retrieving the top apps from a host, I was doubting in between a sorted insertion of models or sorting them on demand. I made the assumption, what could be wrong, of expecting more changes than retrievals, so I finally chose to sort them on call.


### About styles and CSS architecture

Considering your job description is aiming for someone able to help on build your next generation of UI components, I chose to organize the exercise styles into some architecture that fits the purpose. On this case, I used [HaitiCSS](https://haiticss.io/), which is mainly based on ITCSS, because it helps on defining the chain of responsibilities for each styled property. At the end may look like a bit overenginereed for resolving the exercise itself, but the compiled result is pretty competitive, and the responsibilities are much clear.


### About Helvetica Neue

In the provided mockups, you requested for Helvetica Neue font familiy, but unfortunately, your choice is a proprietary font which cannot be hosted or distributed without buying the proper license. Considering the purpose of the exercise, I preferred to choose an open similar alternative, on this case, I used [Arimo](https://fonts.google.com/specimen/Arimo) instead.
